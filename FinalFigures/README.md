# Dark Flow Figures:
## Searching
* z-residual vs angular distance from bulk flow direction
## Tests
* Detection vs input magnitudes
* Direction vs input magnitude
* Hist of detected magnitude (if no bulk flow is present)

# Data sets:
## Union2.1 z < 0.05
## Union2.1 z > 0.05
* scatter shows that its not there. Don't worry about tests (at least not yet)
## SDSS all
## SDSS z < 0.15?
* this is the most important (?)
## SDSS w/ bulk flow at edge of stripe
* only do tests (note that hist will be the same)