import numpy as np 
import matplotlib.pyplot as plt 

def sample_size(v_bf, z=0.3):
	mu_error = (v_bf/299792.0)*(5.0/np.log(10.0))*((1.0+z)/(z*(1.0+z/2.0)))
	# return mu_error
	# return 1.0/(1.48*mu_error)**2 #1/1.48^2 looks good! but how do you get it?
	return (0.2**2)/(0.09*mu_error**2) #14.0/mu_error**2 #this is the required number of SN if mu_error = 0.2 goes to 350 SN
#2.64
# print sample_size(325.0, 0.17)*np.sqrt(191.0)
# print 0.2**2/(191*sample_size(325, 0.05)**2)
# from sys import exit
# exit()

z = np.linspace(0.005, 0.35, 1000)
v_bf = np.linspace(50, 2500, 1000)
#find mu_error required to see a 300 km/s bulk flow at this distance.
mu_error = map(sample_size, v_bf, 0.2*np.ones(1000))

print sample_size(325), sample_size(325, 0.05), 14.0/0.2**2, 14.0/0.02**2

# x = np.linspace(0.01, 0.5, 1000)
# ytop = 14.0/(0.2*np.ones(1000))**2
# ybottom = 14.0/(0.008*np.ones(1000))**2
# ybottom = 14.0/(0.02*np.ones(1000))**2
# ytop = 15000*np.ones(1000) #from sample_size(0.3) with just order of magnitude
# ybottom = 578*np.ones(1000) #Union2.1 N_sn(z<0.05) to one order of sig figs.

# print z
plt.figure()
ax = plt.axes()

#update fonts, make it LaTeX-ish -- http://stackoverflow.com/questions/21461155/change-matplotlibs-default-font
# matplotlib.rc('font', family='serif') 
# matplotlib.rc('font', serif='Courier')#serif='Computer Modern Roman')    #no idea why this breaks it the code. 
# matplotlib.rcParams.update({'font.size': 14})

# plt.rcParams.update({'font.family': 'serif'})

plt.semilogy(v_bf, map(sample_size, v_bf, 0.3*np.ones(1000)), label='z = 0.3', lw=2)
plt.semilogy(v_bf, map(sample_size, v_bf, 0.1*np.ones(1000)), label='z = 0.1', c='m', ls='--', lw=2)
plt.semilogy(v_bf, map(sample_size, v_bf, 0.05*np.ones(1000)), label='z = 0.05', c='c', ls='-.', lw=2)

# ax.arrow(300, ybottom[0]+50, 0.0, 8000, head_width=80, head_length=5000.0, linewidth=2, fc='k', ec='k')

# plt.text(1000, ytop[0]+5000, "goal")	#was (0.4, 0.0135)
# plt.plot(v_bf, ytop, 'k--')

plt.text(615, 191+50, "Union2.1 with z < 0.05", fontsize=10) #was (0.1, 0.2025)
# plt.plot(v_bf, ybottom, 'k--')
plt.axhline(191, c='k', ls='--')

# plt.plot(300*np.ones(1000), np.logspace(0,1000000,1000), 'k--')
plt.text(346, 10, '326 km s$^{-1}$', fontsize=10)#, rotation=90)
plt.axvline(326, c='k', ls='dashed')


# matplotlib.rc('font', family='serif') 
# plt.title('at z = 0.3')
# plt.xlabel(r'max dark flow visible [km s$^{-1}$]') #change to max flow possible, Peter likes words
plt.xlabel(r'dark flow velocity [km s$^{-1}$]') #this is what I gave mathews, and he has not complained.
plt.ylabel('number of SN needed')
plt.legend(loc=0)

plt.savefig('WhatDataSetSize.pdf')
plt.show()