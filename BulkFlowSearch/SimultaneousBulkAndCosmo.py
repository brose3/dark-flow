#!/Users/No+va/anaconda/bin/python
##############################
#Creates: 
#Written by Ben Rose
#brose3@nd.edu
#On 2013-11-25  
#Notes: 
##############################
import cosmolopy as cp
import cosmolopy.distance as csd
import minuit
import scipy.optimize as opt
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import sys
import time
# import pdb
##############################


############################## What Data & Cuts ############################## 
z_max = 2 #0.05
z_min = 0.05 #0.003 is lowest in Union2.1 with low z cut accepted.
global issue_res_value_global
issue_res_value_global = -0.02
date = time.strftime("%Y-%m-%d-%H%M%S") 
crc = True
if crc:
	home = '/afs/crc.nd.edu/user/b/brose3/Private/dark-flow/BulkFlowSearch/'
else:
	home = ''

test = False

#dataset_from='SDSSII'
#my_data = np.recfromtxt('DataSets/sdss-ii_lb.csv',  delimiter=",") #for SDSS-ii data
#dataset_from='SDSSIIcmb'
#my_data = np.recfromtxt('DataSets/sdss-ii_lb_cmb.csv',  delimiter=",") #for SDSS-ii data
#dataset_from='SDSSIIcmb_0.15'
#my_data = np.recfromtxt('DataSets/sdss-ii_lb_cmb_0.15.csv',  delimiter=",")
#dataset_from='Constitution'
#my_data = np.recfromtxt('DataSets/Constitution_Final.csv',  delimiter=",") #for Constitution data
dataset_from='Union'
my_data = np.recfromtxt(home+'DataSets/Union2.1_wLowZ.csv',  delimiter=",") #for Union data

#dataset_from='UnionBulked2000'
#my_data = np.recfromtxt('DataSets/Union2.1_w2000BF.csv',  delimiter=",") #for Union data
#dataset_from='UniformDist2000_500SN'
#my_data = np.recfromtxt('DataSets/z_cut_1_mu_scatter_0.23_bf_2000_sn_500.csv',  delimiter=",")
#dataset_from='UniformDist2000_3920SN'
#my_data = np.recfromtxt('DataSets/z_cut_1_mu_scatter_0.23_bf_2000_sn_3920.csv',  delimiter=",")

# dataset_from='Perfect0.05'
# my_data = np.recfromtxt(home+'DataSets/z_cut_1_mu_scatter_0.05_bf_0_sn_500.csv',  delimiter=",")


# test = True

# dataset_from='Perfect_1_1500'
# my_data = np.recfromtxt(home+'DataSets/Perfect_lowerScatter/2014-08-18-144140-Data-Sample1-BulkFlow1500.csv',  delimiter=",")
# dataset_from='Perfect_2_3000'
# my_data = np.recfromtxt(home+'DataSets/Perfect_lowerScatter/2014-08-18-144140-Data-Sample2-BulkFlow3000.csv',  delimiter=",")

# dataset_from='Best_0_0'
# my_data = np.recfromtxt(home+'DataSets/Best_lowerZ/2014-08-18-143846-Data-Sample0-BulkFlow0.csv',  delimiter=",")

##############################


############################## FUNCTIONS ############################## 
def chi_sqrd_cosmo(OMfit_0, h_fit_0):                     #define chi-squared
	Omega_Lamda_fitting_0=1-OMfit_0             #OMfit_0 is short for Omega_Matter_fitting_0
	#print OMfit_0, Omega_Lamda_fitting_0, h_fit_0
	cosmo = {'omega_M_0' : OMfit_0, 'omega_lambda_0' : Omega_Lamda_fitting_0, 'h' : h_fit_0}
	cosmo = csd.set_omega_k_0(cosmo) 
	mu_theory = cp.magnitudes.distance_modulus(z_observed_np_global, **cosmo)
	p = sum( (mu_observed_np_global-mu_theory)**2/(mu_error_np_global**2) )
	#print p 
	return p
def function(l, b, z_bulk, OMfit_0, h_fit_0):
	############################## needed functions inside
	def cos_angdist(n_bulk,data_sn, unitflag=0):
		#unitflag 1 means degrees
		if unitflag == 1:
			n_bulk[0] = n_bulk[0]* np.pi/180.0
			n_bulk[1] = n_bulk[1] * np.pi/180.0
		#Get cosine(angular distance)
		dots = []
		for i in data_sn:
			dots.append( dotproduct( i, n_bulk) )
		return np.array( dots )
	def dotproduct( data, pole, unitflag=0 ):
		#unitflag 1 means degrees, 0 means radians
		return np.cos( offset( unitflag, data[0],pole[0],data[1],pole[1]) )
	def offset(unitflag,ra1,ra2,dec1,dec2):
		#unitflag 1 means degrees
		if unitflag == 1:
			ra1 = ra1 * np.pi/180.0
			ra2 = ra2 * np.pi/180.0
			dec1 = dec1 * np.pi/180.0
			dec2 = dec2 * np.pi/180.0
		deldec = dec2 - dec1
		delra  = ra2 - ra1
		sindis = np.sqrt(np.sin(deldec/2.0)*np.sin(deldec/2.0) + np.cos(dec2)*np.cos(dec1)*np.sin(delra/2)*np.sin(delra/2))
		dis = 2.0*np.arcsin(sindis)
		ang = np.arctan(np.sin(delra) / (np.cos(dec1)*np.tan(dec2) - np.sin(dec1)*np.cos(delra)))
		offe = dis * np.sin(ang)
		offn = dis * np.cos(ang)
		offt = np.sqrt(offe**2 + offn**2)                   #off = np.array( offe* (180/pi) * 3600.0,offn* (180/pi) * 3600.0,offt )
		off = np.array( offt )
		return off
	############################## set up variables
	cosmo = {'omega_M_0' : OMfit_0, 'omega_lambda_0' : 1-OMfit_0, 'h' : h_fit_0}
	cosmo = csd.set_omega_k_0(cosmo)
	pole = (l, b)
	#print OMfit_0, h_fit_0, pole, z_bulk
	############################## Solve for fit_res_np, the value of the cosine fit at each SN's angular distance
	fit_res_np = z_bulk*cos_angdist(pole, sn_position_global)
	
	############################## Solve for z_res_np_global, realy residue from bulk flow (Davis 2011 eq 15).
	def find_z_res(z_func, *args):                         #args: mu_func, z_func, and cosmology (but cosmo works)
		#z_func = z_func + 0.501        
		#if z_func < 0.00334:
			#z_func  = 0.00334
			#print 'FAIL'
		mu_theory_fnczfit = cp.magnitudes.distance_modulus(z_func, **cosmo)
		return (mu_theory_fnczfit - args[0] + 10*np.log10((1+args[1])/(1+z_func)))**2
	global z_res_np_global
	z_res_np_global = np.zeros(len(z_observed_np_global))
	z_cosmo_measured_np = np.zeros(len(z_observed_np_global))      
	for i, muz in enumerate(zip(mu_observed_np_global,z_observed_np_global)):
		arguments = (muz[0], muz[1], cosmo) 
		#z_cosmo_measured_np[i] = opt.newton(find_z_res, muz[1],  tol=1.48e-07, maxiter=200, args=arguments) #droped tol by 10 and increaced maxiter from 50 to get it to work with real set
		#z_cosmo_measured_np[i] = opt.brentq(find_z_res, -0.5, 0.5, args=arguments) #bceause newton keeps guessing negative z values, try range, muz[1]+/- epsilon?
		zeroed = opt.minimize_scalar(find_z_res, bounds = (0.004, 1), args= arguments, method = 'Bounded') #    
		z_cosmo_measured_np[i] = zeroed.x
		#z_cosmo_measured_np[i] = opt.fsolve(find_z_res, muz[1], args=arguments) #ran for hours and then failed, unsure why. 
	z_res_np_global = (1+z_observed_np_global)/(1+z_cosmo_measured_np)-1 
	
	############################## Calculate chi-squared
	mu_to_z_error_np = mu_error_np_global * (np.log(10)/5)*(z_cosmo_measured_np*(1+z_cosmo_measured_np/2)/(1+z_cosmo_measured_np)) #using z_error over mu_error from A4 in Davis 2011
	chi_z_error_np = mu_to_z_error_np**2 #+ (300/299792)**2 # add in quadrature a 300 km/s z error to deweight any peculiar motion.
	global outlier_global
	outlier_global = ((fit_res_np-z_res_np_global)**2/chi_z_error_np)
	chi_squared = sum((fit_res_np-z_res_np_global)**2/chi_z_error_np) #chi_squared of res, but is denomiator correct?

	## save data for debuging results
	issues_list_all = [0]*10
	for i, value in enumerate(z_res_np_global):
		#if abs(value) > abs(issue_res_value_global):
		if value < issue_res_value_global:
			issues_list = [l_observed_np_global[i], b_observed_np_global[i], z_observed_np_global[i], z_error_np_global[i], mu_observed_np_global[i], mu_error_np_global[i], fit_res_np[i], value, chi_z_error_np[i], outlier_global[i]] #must change inital lenght of issues_list_all 
			issues_list_all = np.vstack((issues_list_all, issues_list))
	global issues_np_global
	issues_np_global = np.array(issues_list_all[1:])
	global fulldata_np_global
	fulldata_np_global = np.dstack([l_observed_np_global, b_observed_np_global, z_observed_np_global, z_error_np_global, mu_observed_np_global, mu_error_np_global, fit_res_np, z_res_np_global, chi_z_error_np, outlier_global])
	
	return chi_squared
def cos_angdist(n_bulk,data_sn, unitflag=0):
	#unitflag 1 means degrees
	if unitflag == 1:
		n_bulk[0] = n_bulk[0]* np.pi/180.0
		n_bulk[1] = n_bulk[1] * np.pi/180.0
	#Get cosine(angular distance)
	dots = []
	for i in data_sn:
		dots.append( dotproduct( i, n_bulk) )
	return np.array( dots )
def dotproduct( data, pole, unitflag=0 ):
	#unitflag 1 means degrees, 0 means radians
	return np.cos( offset( unitflag, data[0],pole[0],data[1],pole[1]) )
def offset(unitflag,ra1,ra2,dec1,dec2):
	#unitflag 1 means degrees
	if unitflag == 1:
		ra1 = ra1 * np.pi/180.0
		ra2 = ra2 * np.pi/180.0
		dec1 = dec1 * np.pi/180.0
		dec2 = dec2 * np.pi/180.0
	deldec = dec2 - dec1
	delra  = ra2 - ra1
	sindis = np.sqrt(np.sin(deldec/2.0)*np.sin(deldec/2.0) + np.cos(dec2)*np.cos(dec1)*np.sin(delra/2)*np.sin(delra/2))
	dis = 2.0*np.arcsin(sindis)
	ang = np.arctan(np.sin(delra) / (np.cos(dec1)*np.tan(dec2) - np.sin(dec1)*np.cos(delra)))
	offe = dis * np.sin(ang)
	offn = dis * np.cos(ang)
	offt = np.sqrt(offe**2 + offn**2)                   #off = np.array( offe* (180/pi) * 3600.0,offn* (180/pi) * 3600.0,offt )
	off = np.array( offt )
	return off
############################## 


############################## Get ussable variables ##############################
## Format Data
l_hold_list=[] #hold
b_hold_list=[]
z_observed_hold_list=[] #observed, hold
z_error_hold_list=[] #error, hold
mu_observed_hold_list=[] #observed, hold
mu_error_hold_list=[] #error, hold
if test == False:
	for i in my_data:
		if i[4] > z_max:
			continue
		elif i[4] < z_min:
			continue
		elif i[7] > 0.4: #only take distance error less then 0.4 mag
			continue
		else:
			if i[1] > np.pi:                            #l
				l_hold_list.append(  i[1] -2* np.pi)
			else:
				l_hold_list.append(  i[1] )
			b_hold_list.append(  i[2] )                 #b
			z_observed_hold_list.append( i[4] )         #z
			if  i[5] == 0:                              #z error
				z_error_hold_list.append( 0.000001)
			else:
				z_error_hold_list.append( i[5] )
			mu_observed_hold_list.append( i[6] )        #mu
			if i[7]==0:                                 #mu_error
				mu_error_hold_list.append( 0.000001)
			else:
				mu_error_hold_list.append( i[7] )
elif test == True:
	for i in my_data:
		if i[2] > z_max:
			continue
		elif i[2] < z_min:
			continue
		elif i[5] > 0.4: #only take distance error less then 0.4 mag
			continue
		else:
			if i[0] > np.pi:                            #l
				l_hold_list.append(  i[0] -2* np.pi)
			else:
				l_hold_list.append(  i[0] )
			b_hold_list.append(  i[1] )                 #b
			z_observed_hold_list.append( i[2] )         #z
			if  i[3] == 0:                              #z error
				z_error_hold_list.append( 0.000001)
			else:
				z_error_hold_list.append( i[3] )
			mu_observed_hold_list.append( i[4] )        #mu
			if i[5]==0:                                 #mu_error
				mu_error_hold_list.append( 0.000001)
			else:
				mu_error_hold_list.append( i[5] )

## check lenghts
if len(z_observed_hold_list) != len(l_hold_list):
	sys.exit("z not same lenght as l")
if len(z_observed_hold_list) < 20:
	sys.exit(("less then 20 SN", len(z_observed_hold_list)))
print "The total number of SN that passed the z cut: ", len(z_observed_hold_list)

## Define global numpy arrays for data
global l_observed_np_global
global b_observed_np_global
global z_observed_np_global
global z_error_np_global
global mu_observed_np_global
global mu_error_np_global
global sn_position_global                                  #make duple for of SN position, to be used for dotproduct fuction.
l_observed_np_global = np.array(l_hold_list)
b_observed_np_global = np.array(b_hold_list)
z_observed_np_global = np.array(z_observed_hold_list)
z_error_np_global = np.array(z_error_hold_list)
mu_observed_np_global = np.array(mu_observed_hold_list)
mu_error_np_global = np.array(mu_error_hold_list)
sn_position_global = zip(l_observed_np_global, b_observed_np_global) 

## Plot acpeted data to look for outlier.
fig1 = plt.figure()
ax1 = fig1.add_axes([0.1,0.1,0.8,0.8], projection='aitoff')   #lambert,aitoff
plt.grid("on")
plt.scatter(l_observed_np_global,b_observed_np_global,c=z_observed_np_global)#, cmap = cm.jet)
plt.xlabel(r'$l$')
plt.ylabel(r'$b$')
plt.colorbar()
plt.savefig(home+'Results/'+date+'DataUsed.pdf')
############################## 

'''
#todo: inital cosmo does not get set corretly, so I am skipping it for now.
############################## Get initial Cosmology ############################## 
mcosmo = minuit.Minuit(chi_sqrd_cosmo, tolerance = 0.00001 )       #minuit can only take variable with name of lenght 10 char.
mcosmo.limits["OMfit_0"]=(0,1)                           #OMfit_0 is hor for Omega_Matter_fitting_0
mcosmo.limits["h_fit_0"] = (0.6,0.88)                    #a standard is 0.72
mcosmo.values["OMfit_0"]= 0.20
mcosmo.values["h_fit_0"] = 0.8
#m.tol = 0.00001                                    #was default 0.1 and "Minimization is successful when the estimated vertical distance to the minimum is 0.001*tol*up" up=1.
mcosmo.migrad()
print "chi-square of cosmo only fit ", mcosmo.fval
print "final inital cosmology", mcosmo.values["OMfit_0"], 1-mcosmo.values["OMfit_0"], mcosmo.values["h_fit_0"]

## Set Cosmology
OM_final_0, OL_final_0, h_final_0 = mcosmo.values["OMfit_0"], 1-mcosmo.values["OMfit_0"], mcosmo.values["h_fit_0"]                     #for a full run or
#OM_final_0, OL_final_0, h_final_0 = 0.313, 0.687, 0.704       #hardcoded cosmology
#cosmo = {'omega_M_0' : OM_final_0, 'omega_lambda_0' : OL_final_0, 'h' : h_final_0}
#cosmo = csd.set_omega_k_0(cosmo) 
##############################
'''

############################## Minimize spread around z_res vs angular dist cosine function ############################## 
fullmin = minuit.Minuit(function, fix_OMfit_0=True, fix_h_fit_0=True)
fullmin.up = 1 #1 sigma changes result by up, use 0.001? if using mu_error in chi-square not a good z_error
fullmin.limits["l"] = (-np.pi, np.pi)
fullmin.limits["b"] = (-np.pi/2, np.pi/2)
fullmin.limits["z_bulk"] = (0, 10)
fullmin.limits["OMfit_0"] = (0.2,0.4)
fullmin.limits["h_fit_0"] = (0.6,0.88)
#from 2015-09-16 run of all Union2.1 OM = 0.341908029832 & h = 0.707129920506
fullmin.values["OMfit_0"] = 0.34 #0.20 #OM_final_0 #mcosmo.values["OMfit_0"] 
fullmin.values["h_fit_0"] = 0.707 #0.8 #h_final_0 #mcosmo.values["h_fit_0"]
fullmin.values["l"] = -1.064650844
fullmin.values["b"] = 0.314159265
fullmin.values["z"] = 0.004
runNumerFlag = 0
try:
	fullmin.strategy=2
	fullmin.tol = 1 #0.1 is default
	#todo: make a file output with input data (intial values of cosmo and guesses), and say it failed to converge. This will be overwriting by sucessful funs later.
	fullmin.printMode = 1 #0 is no output, 1 is fcn & parameter values at each call
	fullmin_error='no error'
	runNumerFlag += 1
	sucess = False
	fullmin.migrad()
	sucess = True
except minuit.MinuitError as me:
	if me == "Function value is above the specified estimated distance to the minimum (edm).":
		while runNumerFlag < 4:
			try:
				fullmin.tol = 10*fullmin.tol
				runNumerFlag += 1
				fullmin.migrad()
				sucess = True
				break
			except minuit.MinuitError as me:
				continue
			except Exception as othererror:
				fullmin_error = str(othererror)
				break
	else:
		fullmin_error = str(me)
except Exception as othererror:
	fullmin_error = str(othererror)

## results	
# pdb.set_trace()
bulk_direction = [fullmin.values["l"], fullmin.values["b"] ]
if sucess:
	one_sig = fullmin.contour("l","b", 1, npoints=100)
	two_sig = fullmin.contour("l","b", 2, npoints=100)
# print "z from ", z_max, "from: ", dataset_from
# print "total number of SN used: ", len(z_observed_hold_list)
# print fullmin_error
# print 'final comology is ', fullmin.values["h_fit_0"], fullmin.values["OMfit_0"]," &", 1-fullmin.values["OMfit_0"]
# print "bulk direction is ", np.degrees(bulk_direction)," in galactic coordinates"
# print "bulk velocity is ", fullmin.values["z_bulk"]*299792," +- ", fullmin.errors["z_bulk"]*299792, " km/s"
# print "error are: ", fullmin.errors
# print "outlier? ", np.sort(outlier_global)
result = (
	"z from " + str(z_min)+ " to "+ str(z_max)+", from"+str(dataset_from),
	"# of SN: "+str(len(z_observed_hold_list)),
	"error: "+ str(fullmin_error),
	"tolerance value: "+ str(fullmin.tol),
	"cosmology: "+ str(fullmin.values["h_fit_0"]) +', '+ str(fullmin.values["OMfit_0"])+', & '+str(1-fullmin.values["OMfit_0"]),
	"bulk direction: "+ str(np.degrees(bulk_direction))+" in galactic coordinates",
	"bulk velocity: "+ str(fullmin.values["z_bulk"]*299792)+" +- "+ str(fullmin.errors["z_bulk"]*299792)+ " km/s, "+str(fullmin.values["z_bulk"]),
	"errors are: "+ str(fullmin.errors),
	"final chi-square value: "+ str(fullmin.fval),
	"outlier? "+str(np.sort(outlier_global)),
	"detailed issues " + str(issues_np_global)#,
	# "1 sigma contour:" + str(one_sig),
	# "2 sigma contour:" + str(two_sig)
	)
resultshort = (
	str(z_min)+' '+ str(z_max)+' '+str(dataset_from), 
	str(len(z_observed_hold_list)),
	str(fullmin_error),
	str(fullmin.tol),
	str(fullmin.values["h_fit_0"]) + " " + str(fullmin.values["OMfit_0"]) +" & "+str(1-fullmin.values["OMfit_0"]),
	str(np.degrees(bulk_direction)),
	str(fullmin.values["z_bulk"]*299792)+" +- "+str(fullmin.errors["z_bulk"]*299792)+" km/s, "+str(fullmin.values["z_bulk"]),
	str(fullmin.errors),
	str(fullmin.fval),
	str(np.sort(outlier_global)),
	str(issues_np_global)#,
	# str(one_sig),
	# str(two_sig)
)
for res in result:
	print res
resultsaveto = home+'Results/'+date+'-resultsfrom'+dataset_from+'-zCut'+str(z_max)+'.txt'
np.savetxt(resultsaveto, resultshort, fmt="%s")
############################## 

############################## Plot the z_res*cos(angle dist) and fit ##############################   
#todo use this to find apmlitude/bulk flow speed
SN_theta_plot = np.arccos(cos_angdist(bulk_direction, sn_position_global))
np.savetxt(home+'Results/'+date+'-finaldata'+dataset_from+'-zCut'+str(z_max)+'.csv',fulldata_np_global.tolist()[0], delimiter = ',')
np.savetxt(home+'Results/'+date+'-plotfrom'+dataset_from+'-zCut'+str(z_max)+'.csv', np.dstack((SN_theta_plot, z_res_np_global)).tolist()[0], delimiter = ',')       

## Plot minimized results
fig2 = plt.figure() #fig1 is dataset used
#x-axis => angular dist of SN from bulk flow
#y-axis => z_res, sn
plt.scatter(SN_theta_plot, z_res_np_global, c=z_observed_np_global)#, cm.jet )
plt.colorbar()
#x-axis => angle
#y-axis => cos(angle)
theta_plot = np.arange(0,np.pi, 0.01)
cosine_plot = (fullmin.values["z_bulk"])*(np.cos(theta_plot))
plt.plot(theta_plot, cosine_plot)
plt.xlabel(r'angular distance on the sky (radians)', fontsize=15)
plt.ylabel(r'z$_{residue}$ (observed - hubble)', fontsize=15)
plt.savefig(home+'Results/'+date+'ScatterPlot.pdf')

## Plot acpeted data to look for outlier.
fig3 = plt.figure()
ax1 = fig3.add_axes([0.1,0.1,0.8,0.8], projection='aitoff')   #lambert,aitoff
plt.grid("on")
plt.scatter(l_observed_np_global,b_observed_np_global,c=outlier_global)#, cmap = cm.jet)
plt.xlabel(r'$l$')
plt.ylabel(r'$b$')
plt.colorbar()
plt.savefig(home+'Results/'+date+'OutlierPlot.pdf')

if sucess:
	print one_sig
	print two_sig
	lplot1, bplot1 = zip(*one_sig) #unlinks the x & y lists inorder to plot
	lplot2, bplot2 = zip(*two_sig)
	print lplot1, bplot1
	print lplot2, bplot2
	
	fig4 = plt.figure()
	ax1 = fig4.add_axes([0.1,0.1,0.8,0.8], projection='aitoff')
	plt.grid("on")
	plt.plot(lplot1,bplot1, 'k')
	plt.plot(lplot2,bplot2, 'k')
	ax1.fill_between(lplot2, 0, bplot2, facecolor='0.6')
	ax1.fill_between(lplot1, 0, bplot1, facecolor='0.3')
	plt.xlabel(r'$l$')
	plt.ylabel(r'$b$')
	plt.savefig(home+'Results/'+date+'LocationCountour.pdf')
##############################

