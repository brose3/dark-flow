Results for YEAR-MONTH-DAY—TIME-resultsfromDATASET.txt in this format:
z_max dataset_name
#_of_SN_passed_zcut
If there is an error or not, added after 2014-01-30-130000
h_0 \Omega_M \Omega_{\Lamda}
BulkDirection
BulkSpeed +- Errors km/s
AllErrors
ChiSquared_final_value

Results for YEAR-MONTH-DAY—TIME-plotfromDATASET.csv in this row* format:
angular distance between bulk flow direction and SN position
z_residue (defined as (1+z_observed)/(1+z_cosmo)-1 & z_cosmo is the z from cosmology assuming a \LamdaCDM + Bulk Flow expansion aka only the spherically symantic part)
*This changed to column format on and after 2014-01-3


General Notes:
* mu_scatter was too low on Uniform Distribution until 2014-02-10