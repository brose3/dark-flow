#!/usr/local/bin/python
##############################
#Creates: No Outputs
#Written by Ben Rose
#brose3@nd.edu
#On 2013-09-02 
#Notes: 
##############################
import cosmolopy.distance as csd
import cosmolopy.magnitudes as csm
import numpy as np
##############################

##############################
# Paramaters
#Bulk Flow
bulk_flow_speed = 2000 #in km/s
bulk_flow_direction = (np.radians(60), np.radians(20)) #in galactic coordinates, radians
#cosmo
#found cosmo of Unioin2.1 upto z=1 and used these numbers.
om_set, ol_set, h_set = 0.2833, 0.7166, 0.6987 #for z<3
#om_set, ol_set, h_set = 0.2938, 0.7061, 0.6979 #for z<1
#om_set, ol_set, h_set = 0.9999, 0.0001, 0.6849 #for z<0.05
#set cosmology
cosmo = {'omega_M_0' : om_set, 'omega_lambda_0' : ol_set, 'h' : h_set}
cosmo = csd.set_omega_k_0(cosmo) 
##############################

##############################
# Import Union2.1
my_data = np.recfromcsv('Union2.1_wLowZ.csv')
## Format Data
name_hold_list=[]
l_hold_list=[] #hold
b_hold_list=[]
space_hold_list=[]
z_union_hold_list=[] #observed, hold
z_error_hold_list=[] #error, hold
mu_observed_hold_list=[] #observed, hold
mu_error_hold_list=[] #error, hold
for i in range(len(my_data)):
	name_hold_list.append( my_data[i][0])
	if my_data[i][1] > np.pi:
		l_hold_list.append( my_data[i][1] -2* np.pi)
	else:
		l_hold_list.append( my_data[i][1] )
	b_hold_list.append( my_data[i][2] )
	space_hold_list.append( my_data[i][3])
	z_union_hold_list.append( my_data[i][4] )
	if my_data[i][5] == 0:
		z_error_hold_list.append( 0.000001)
	else:
		z_error_hold_list.append( my_data[i][5] )
	mu_observed_hold_list.append( my_data[i][6] )
	if my_data[i][7]==0:
		mu_error_hold_list.append( 0.000001)
	else:
		mu_error_hold_list.append( my_data[i][7] )
name_np = np.array(name_hold_list)
l_observed_np = np.array(l_hold_list)
b_observed_np = np.array(b_hold_list)
space_np = np.array(space_hold_list)
z_union_np = np.array(z_union_hold_list)
z_error_np = np.array(z_error_hold_list)
mu_observed_np = np.array(mu_observed_hold_list)
mu_error_np = np.array(mu_error_hold_list)
############################## 

############################## 
# Add Bulk Flow
def cos_angdist(n_bulk,data_sn, unitflag=0):
	#unitflag 1 means degrees
	if unitflag == 1:
		n_bulk[0] = n_bulk[0]* np.pi/180.0
		n_bulk[1] = n_bulk[1] * np.pi/180.0
	#Get cosine(angular distance)
	dots = []
	for i in data_sn:
		dots.append( dotproduct( i, n_bulk) )
	return np.array( dots )
def dotproduct( data, pole, unitflag=0 ):
	#unitflag 1 means degrees, 0 means radians
	return np.cos( offset( unitflag, data[0],pole[0],data[1],pole[1]) )
def offset(unitflag,ra1,ra2,dec1,dec2):
	#unitflag 1 means degrees
	if unitflag == 1:
		ra1 = ra1 * np.pi/180.0
		ra2 = ra2 * np.pi/180.0
		dec1 = dec1 * np.pi/180.0
		dec2 = dec2 * np.pi/180.0
	deldec = dec2 - dec1
	delra  = ra2 - ra1
	sindis = np.sqrt(np.sin(deldec/2.0)*np.sin(deldec/2.0) + np.cos(dec2)*np.cos(dec1)*np.sin(delra/2)*np.sin(delra/2))
	dis = 2.0*np.arcsin(sindis)
	ang = np.arctan(np.sin(delra) / (np.cos(dec1)*np.tan(dec2) - np.sin(dec1)*np.cos(delra)))
	offe = dis * np.sin(ang)
	offn = dis * np.cos(ang)
	offt = np.sqrt(offe**2 + offn**2)#    off = np.array( offe* (180/pi) * 3600.0,offn* (180/pi) * 3600.0,offt )
	off = np.array( offt )
	return off

z_observed = (1 + bulk_flow_speed*cos_angdist(bulk_flow_direction, zip(l_observed_np,b_observed_np))/299792)*(1 + z_union_np) - 1
for i, red in enumerate(z_observed):
	if red<0.00287: #closer thqn Virgo, http://adsabs.harvard.edu/abs/1988A%26A...202...70A
		z_observed[i] = 0.00287
##############################
############################## 

############################## 
# Output results
data = np.dstack((name_np,l_observed_np,b_observed_np,space_np,z_observed,z_error_np,mu_observed_np,mu_error_np))
print data[0]
np.savetxt('Union2.1_w'+str(bulk_flow_speed)+'BF.csv', data[0], delimiter=',',fmt="%s")
print 'made Union2.1_w'+str(bulk_flow_speed)+'BF.csv'
############################## 