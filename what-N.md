For a data set:
$$\sigma_t = \dfrac{\sigma_i}{\sqrt{N}},$$ or
$$N = (\dfrac{\sigma_i}{\sigma_t})^2.$$

