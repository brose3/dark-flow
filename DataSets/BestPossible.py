#!/usr/local/bin/python
#Written by brose3@nd.edu
#On 2013-09-02 
import cosmolopy.distance as csd
import cosmolopy.magnitudes as csm
import numpy as np
import matplotlib.pyplot as plt
from scipy import interpolate

##############################
# functions
##############################
def getb(num):
    return np.arccos(2*np.random.rand(num)-1)-np.pi/2
def makelike(data, leng):
    ''' ouputs and array with the same distribution as the input array, lenghts can be different''' 
    ''' seems not to work with constant distributions'''    
    hist, bins = np.histogram(data)
    bin_midpoints = bins[:-1]+np.diff(bins)/2
    cdf = np.cumsum(hist)
    cdf = 1.0*cdf/cdf[-1] #make it a float between (0,1].
    values = np.random.rand(leng)*(max(cdf)-min(cdf))+min(cdf)
    cdf_inversed_c = interpolate.interp1d(cdf, bin_midpoints) #makes function where C_VALUE = cdf_inversed_c(CDF_VALUE). Check out CDF_test.py
    return cdf_inversed_c(values)


##############################
# Paramaters
##############################
#cosmo
#om_set, ol_set, h_set = 0.313, 0.687, 0.704
om_set, ol_set, h_set = 0.2833, 0.7166, 0.6987 #from Union2.1

#SN info
#number_of_SN = 3920 #to try to match Union2.1 with a z_cut of 0.05
number_of_SN = 500
mu_scatter = 0.2 #to try and match average Union2.1 mu_error
z_cutoff = 0.3
z_min = 0.05 #0.00334 #or 1000km/s 
#z_min = 0.0334


##############################
# Set things up 
##############################
## set cosmology
cosmo = {'omega_M_0' : om_set, 'omega_lambda_0' : ol_set, 'h' : h_set}
cosmo = csd.set_omega_k_0(cosmo) 

## import union2 and get its redshift, redshift errors, and distance modulus error
Union2 = np.recfromcsv('Union2.1_wLowZ.csv')
### make lists/np.arrays of the data
z_hold_list=[]
z_error_hold_list=[]
mu_error_hold_list=[]
for sn_detail in Union2:
    if sn_detail[7] > 0.4: #only take distance error less then 0.4 mag
        continue
    else:
        z_hold_list.append( sn_detail[4])
        if sn_detail[5] == 0:
            z_error_hold_list.append( 0.0001)
        else:
            z_error_hold_list.append( sn_detail[5] )
        if sn_detail[7]==0:
            mu_error_hold_list.append( 0.0001)
        else:
            mu_error_hold_list.append( sn_detail[7] )
Union_z = np.array(z_hold_list)
Union_zerr = np.array(z_error_hold_list)
Union_muerr = np.array(mu_error_hold_list)


##############################
# Made data set
##############################
## Uniform sky coverage excep for galactic plane (all in galactic coordinates)
### make l
l = 2*np.pi*np.random.rand(number_of_SN) - np.pi #produces [-pi, pi)
### make b
b = getb(number_of_SN) #produces [-pi/2, pi/2) weighted around zero
### cut and replace b inside galactic plane
for i, lat in enumerate(b):
    while lat < np.radians(15) and lat > np.radians(-15):
        hold = getb(1)[0]
        lat = hold
    b[i] = lat 

## make z cosmo from union2 distribution
### random z
#z = np.random.rand(number_of_SN)*(z_cutoff-z_min)+z_min #ranodm is uniform over[z_min,z_cutoff)
### Union2 like z distribution
z = makelike(Union_z, number_of_SN)
## make z error from Union2 distribution
### make like union2
# z_error = makelike(Union_zerr, number_of_SN) #producing negative values?
### just fill in with 0.001
z_error = np.empty(number_of_SN)
z_error.fill(0.001)
## make mu_error from Union2
mu_error = makelike(Union_muerr, number_of_SN)

## get mu's with error
mu_true = csm.distance_modulus(z, **cosmo) 
mu = mu_true+np.random.normal(0,mu_error, number_of_SN) # not going to work.
#think about adding in (300km/s)/zc in quadrature to account for random motion in close stuff.


##############################
# Export
##############################
#create and print final dataset
number = np.empty(number_of_SN)
number.fill(1)
#index,l, b, #, z, error, mu, error
data = np.dstack((number,l,b,number,z,z_error,mu,mu_error))
print data[0]
np.savetxt('Best_'+str(number_of_SN)+'.csv',data[0], delimiter=',')


#plot
fig1 = plt.figure()
#ax = fig1.add_subplot(111)
ax1 = fig1.add_axes([0.1,0.1,0.8,0.8], projection='aitoff')   #lambert,aitoff
plt.grid("on")
plt.scatter(l,b,c=z)
plt.xlabel(r'$l$')
plt.colorbar()
plt.savefig('AllSky_Best.pdf')

## plot hists to check
fig2 = plt.figure()
# plt.hist(z)
plt.hist(z_error) #producecs negative results
# plt.hist(Union_zerr) #is just constant!
# plt.hist(mu_error)
# plt.show()

# fig2 = plt.figure()
# #ax = fig1.add_subplot(111)
# ax1 = fig2.add_axes([0.1,0.1,0.8,0.8], projection='aitoff')   #lambert,aitoff
# plt.grid("on")
# plt.scatter(l,b,c=z_ob, cmap = cm.jet)
# plt.xlabel(r'$l$')
# plt.ylabel(r'$b$')
# colorbar()
# plt.savefig('AllSky_z_with_z_cut_'+str(z_cutoff)+'_mu_scatter_'+str(mu_scatter)+'_bf_'+str(bulk_flow_speed)+'.pdf')
# print 'made file AllSky_z_with_z_cut_'+str(z_cutoff)+'_mu_scatter_'+str(mu_scatter)+'_bf_'+str(bulk_flow_speed)+'.pdf'
############################## 