#!/usr/local/bin/python
##############################
#Creates: 
#Written by Ben Rose
#brose3@nd.edu
#On 2014-04-02 
#Notes: 
##############################
import numpy as np

def HelioToCMB(v, l, b, degrees = True):
	#http://ned.ipac.caltech.edu/help/velc_help.html
	vapex, lapex, bapex = 371.0, 264.14, 48.26 #[km/s, deg, deg]
	#everything needs to be raddians for trig functions. 
	lapex, bapex = np.radians(lapex), np.radians(bapex)
	if degrees:
		l = np.radians(l)
		b = np.radians(b)
		val = v + vapex*( np.sin(b)*np.sin(bapex)+np.cos(b)*np.cos(bapex)*np.cos(l-lapex) )
	else:
		val = v + vapex*( np.sin(b)*np.sin(bapex)+np.cos(b)*np.cos(bapex)*np.cos(l-lapex) )
	return val

def HelioToCMB_test():
	v = np.array([1000,1500,2000])
	l = np.array([90,75,40])
	b = np.array([22,44,0])
	print HelioToCMB(v,l,b)
	l = np.radians(np.array([90,75,40]))
	b = np.radians(np.array([22,44,0]))
	print HelioToCMB(v,l,b,degrees=False)
	return


##############################
# Import Union2.1
my_data = np.recfromcsv('sdss-ii_lb.csv')

## Split data into lists
name_list=[]
l_list=[] #hold
b_list=[]
space_list=[]
z_list=[] #observed, hold
z_error_list=[] #error, hold
mu_list=[] #observed, hold
mu_error_list=[] #error, hold
for i in range(len(my_data)):
	name_list.append( my_data[i][0])
	if my_data[i][1] > np.pi:
		l_list.append( my_data[i][1] -2* np.pi)
	else:
		l_list.append( my_data[i][1] )
	b_list.append( my_data[i][2] )
	space_list.append( my_data[i][3])
	z_list.append( my_data[i][4] )
	if my_data[i][5] == 0:
		z_error_list.append( 0.000001)
	else:
		z_error_list.append( my_data[i][5] )
	mu_list.append( my_data[i][6] )
	if my_data[i][7]==0:
		mu_error_list.append( 0.000001)
	else:
		mu_error_list.append( my_data[i][7] )

## convert to np.array
name_np = np.array(name_list)
l_np = np.array(l_list)
b_np = np.array(b_list)
space_np = np.array(space_list)
z_helio_np = np.array(z_list)
z_error_np = np.array(z_error_list)
mu_np = np.array(mu_list)
mu_error_np = np.array(mu_error_list)

## convert to CMB frame
z_np = HelioToCMB(z_helio_np*299792, l_np, b_np, degrees = False)/299792
############################## 

data = np.dstack((name_np, l_np, b_np, space_np,z_np,z_error_np,mu_np,mu_error_np))
np.savetxt('sdss-ii_lb_cmb.csv', data[0], delimiter=',',fmt="%s")
print my_data[0]
print data[0][0]
print 'made sdss-ii_lb_cmb.csv'
