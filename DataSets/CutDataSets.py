##############################
#Creates: 
#Written by Ben Rose
#brose3@nd.edu
#On 2014-04-04  
#Notes: 
##############################
import numpy as np
import sys
##############################

############################## Get ussable variables ##############################
z_max = 0.15 
z_min = 0.00334 
my_data = np.recfromtxt('sdss-ii_lb_cmb.csv',  delimiter=",") 
## Format Data
l_hold_list=[] #hold
b_hold_list=[]
z_observed_hold_list=[] #observed, hold
z_error_hold_list=[] #error, hold
mu_observed_hold_list=[] #observed, hold
mu_error_hold_list=[] #error, hold
for i in my_data:
	if i[4] > z_max:
		continue
	elif i[4] < z_min:
		continue
	else:
		if i[1] > np.pi:                            #l
			l_hold_list.append(  i[1] -2* np.pi)
		else:
			l_hold_list.append(  i[1] )
		b_hold_list.append(  i[2] )                 #b
		z_observed_hold_list.append( i[4] )         #z
		if  i[5] == 0:                              #z error
			z_error_hold_list.append( 0.000001)
		else:
			z_error_hold_list.append( i[5] )
		mu_observed_hold_list.append( i[6] )        #mu
		if i[7]==0:                                 #mu_error
			mu_error_hold_list.append( 0.000001)
		else:
			mu_error_hold_list.append( i[7] )

## check lenghts
if len(z_observed_hold_list) != len(l_hold_list):
	sys.exit("z not same lenght as l")
if len(z_observed_hold_list) < 20:
	sys.exit(("less then 20 SN", len(z_observed_hold_list)))
print "The total number of SN that passed the z cut: ", len(z_observed_hold_list)

l = np.array(l_hold_list) 
b = np.array(b_hold_list)
z_ob = np.array(z_observed_hold_list)
z_error = np.array(z_error_hold_list)
mu_ob = np.array(mu_observed_hold_list)
mu_error = np.array(mu_error_hold_list)
number_of_SN = len(l)
number = np.empty(number_of_SN)
number.fill(1)

data = np.dstack((number,l,b,number,z_ob,z_error,mu_ob,mu_error))
np.savetxt('sdss-ii_lb_cmb_'+str(z_max)+'.csv',data[0], delimiter=',')