#!/usr/local/bin/python
##############################
#Creates: No Outputs
#Written by Ben Rose
#brose3@nd.edu
#On 2013-09-02 
#Notes: 
##############################
import cosmolopy.distance as csd
import cosmolopy.magnitudes as csm
import numpy as np
import matplotlib.pyplot as plt
##############################


##############################
# Paramaters
#cosmo
#om_set, ol_set, h_set = 0.313, 0.687, 0.704
om_set, ol_set, h_set = 0.2833, 0.7166, 0.6987 #from Union2.1

#SN info
#number_of_SN = 3920 #to try to match Union2.1 with a z_cut of 0.05
number_of_SN = 500
mu_scatter = 0.05 #to try and match average Union2.1 mu_error
z_cutoff = 1
z_min = 0.00334 #or 1000km/s 
#z_min = 0.0334

#Bulk Flow
bulk_flow_speed = 0 #in km/s
bulk_flow_direction = (np.radians(60), np.radians(20)) #in galactic coordinates, radians
##############################


##############################
# Set things up 
#set cosmology
cosmo = {'omega_M_0' : om_set, 'omega_lambda_0' : ol_set, 'h' : h_set}
cosmo = csd.set_omega_k_0(cosmo) 

#distribute the SN
l = 2*np.pi*np.random.rand(number_of_SN) - np.pi #produces [-pi, pi)
b = np.arccos(2*np.random.rand(number_of_SN)-1)-np.pi/2 #produces [-pi/2, pi/2) weighted around zero

#make random z
z_cosmo = np.random.rand(number_of_SN)*(z_cutoff-z_min)+z_min #ranodm is uniform over[z_min,z_cutoff)

#add bulk flow
def cos_angdist(n_bulk,data_sn, unitflag=0):
    #unitflag 1 means degrees
    if unitflag == 1:
        n_bulk[0] = n_bulk[0]* np.pi/180.0
        n_bulk[1] = n_bulk[1] * np.pi/180.0
    #Get cosine(angular distance)
    dots = []
    for i in data_sn:
        dots.append( dotproduct( i, n_bulk) )
    return np.array( dots )
def dotproduct( data, pole, unitflag=0 ):
    #unitflag 1 means degrees, 0 means radians
    return np.cos( offset( unitflag, data[0],pole[0],data[1],pole[1]) )
def offset(unitflag,ra1,ra2,dec1,dec2):
    #unitflag 1 means degrees
    if unitflag == 1:
        ra1 = ra1 * np.pi/180.0
        ra2 = ra2 * np.pi/180.0
        dec1 = dec1 * np.pi/180.0
        dec2 = dec2 * np.pi/180.0
    deldec = dec2 - dec1
    delra  = ra2 - ra1
    sindis = np.sqrt(np.sin(deldec/2.0)*np.sin(deldec/2.0) + np.cos(dec2)*np.cos(dec1)*np.sin(delra/2)*np.sin(delra/2))
    dis = 2.0*np.arcsin(sindis)
    ang = np.arctan(np.sin(delra) / (np.cos(dec1)*np.tan(dec2) - np.sin(dec1)*np.cos(delra)))
    offe = dis * np.sin(ang)
    offn = dis * np.cos(ang)
    offt = np.sqrt(offe**2 + offn**2)#    off = np.array( offe* (180/pi) * 3600.0,offn* (180/pi) * 3600.0,offt )
    off = np.array( offt )
    return off

z_ob = (1 + bulk_flow_speed*cos_angdist(bulk_flow_direction, zip(l,b))/299792)*(1 + z_cosmo) - 1
for i, red in enumerate(z_ob):
    if red<0.00287: #closer thqn Virgo, http://adsabs.harvard.edu/abs/1988A%26A...202...70A
        z_ob[i] = 0.00287
np.savetxt('zcosmo.csv', z_cosmo ,delimiter=',')

#get mu's with error
mu_true = csm.distance_modulus(z_cosmo, **cosmo) + (10*np.log10(1+ bulk_flow_speed*cos_angdist(bulk_flow_direction, zip(l,b))/299792))
mu = mu_true+np.random.normal(0,mu_scatter,number_of_SN)
#think about adding in (300km/s)/zc in quadrature to account for random motion in close stuff.
##############################


##############################
# Export
#create and print final dataset
z_error = np.empty(number_of_SN)
z_error.fill(0.0001)
mu_error = np.empty(number_of_SN)
mu_error.fill(mu_scatter) 
#mu_error.fill(0.01) 
number = np.empty(number_of_SN)
number.fill(1)
#index,l, b, #, z, error, mu, error
data = np.dstack((number,l,b,number,z_ob,z_error,mu,mu_error))
print data[0]
np.savetxt('z_cut_'+str(z_cutoff)+'_mu_scatter_'+str(mu_scatter)+'_bf_'+str(bulk_flow_speed)+'_sn_'+str(number_of_SN)+'.csv',data[0], delimiter=',')
print 'made file z_cut_'+str(z_cutoff)+'_mu_scatter_'+str(mu_scatter)+'_bf_'+str(bulk_flow_speed)+'_sn_'+str(number_of_SN)+'.csv'

# #plot
# fig1 = plt.figure()
# #ax = fig1.add_subplot(111)
# ax1 = fig1.add_axes([0.1,0.1,0.8,0.8], projection='aitoff')   #lambert,aitoff
# plt.grid("on")
# plt.scatter(l,b,c=mu, cmap = cm.jet)
# plt.xlabel(r'$l$')
# colorbar()
# plt.savefig('AllSky_mu_with_z_cut_'+str(z_cutoff)+'_mu_scatter_'+str(mu_scatter)+'_bf_'+str(bulk_flow_speed)+'.pdf')
# print 'made file AllSky_mu_with_z_cut_'+str(z_cutoff)+'_mu_scatter_'+str(mu_scatter)+'_bf_'+str(bulk_flow_speed)+'.pdf'

# fig2 = plt.figure()
# #ax = fig1.add_subplot(111)
# ax1 = fig2.add_axes([0.1,0.1,0.8,0.8], projection='aitoff')   #lambert,aitoff
# plt.grid("on")
# plt.scatter(l,b,c=z_ob, cmap = cm.jet)
# plt.xlabel(r'$l$')
# plt.ylabel(r'$b$')
# colorbar()
# plt.savefig('AllSky_z_with_z_cut_'+str(z_cutoff)+'_mu_scatter_'+str(mu_scatter)+'_bf_'+str(bulk_flow_speed)+'.pdf')
# print 'made file AllSky_z_with_z_cut_'+str(z_cutoff)+'_mu_scatter_'+str(mu_scatter)+'_bf_'+str(bulk_flow_speed)+'.pdf'
############################## 