import numpy as np
import matplotlib.pyplot as plt

plt.rc('font', family='serif') 

# data = np.genfromtxt('2014-05-27-Output-unionlow.csv', delimiter = ',')
data = np.genfromtxt('2016-03-01-unionlow.csv', delimiter = ',')
plt.figure('low z')
# 2016-03-01 max at 245
plt.hist(data, bins = [0, 25, 50, 75, 100, 125, 150, 175, 200, 225, 250])
plt.xlabel(r'bulk flow velocity [km s$^{-1}$]', fontsize=14)
plt.ylabel('frequency', fontsize=14)
# plt.savefig('union2low_null_newbins.pdf')


# data = np.genfromtxt('Union2_high_null-output.csv', delimiter = ',')
data = np.genfromtxt('2016-03-01-unionhigh.csv', delimiter = ',')
plt.figure('high z')
# 2016-03-01 max at 6385
plt.hist(data, bins = [0, 500, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000, 5500, 6000, 6390])
plt.xlabel(r'dark flow velocity [km s$^{-1}$]', fontsize=14)
plt.ylabel('frequency', fontsize=14)
plt.savefig('union2high_null_newbins.pdf')

#2014-04-10-Output-sdss or 
#2014-04-09-Output-sdss_cmb_best?

# data = np.genfromtxt('2014-04-10-Output-sdss.csv', delimiter = ',')
data = np.genfromtxt('2016-03-28-Output-sdss-corrected.csv', delimiter = ',')
plt.figure('sdss')
plt.hist(data/1000.0, bins = np.array([0, 5000, 10000, 15000, 20000, 25000, 30000, 35000, 39000])/1000.0) #max = 38591
plt.xlabel(r'dark flow velocity [$10^3$ km s$^{-1}$]', fontsize=14)
plt.ylabel('frequency', fontsize=14)
plt.savefig('sdss_null_newbins.pdf')

# plt.show()