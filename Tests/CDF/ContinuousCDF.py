import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
from scipy import interpolate


############################## 
my_data = np.recfromcsv('sdss-ii_lb_cmb.csv')
#my_data = np.recfromcsv('sdss-ii_lb_cmb_0.15.csv')
l_hold_list=[] #hold
b_hold_list=[]
z_hold_list=[]
z_error_hold_list=[] #error, hold
mu_error_hold_list=[] #error, hold
for i in range(len(my_data)):
	if my_data[i][1] > np.pi:
		l_hold_list.append( my_data[i][1] -2* np.pi)
	else:
		l_hold_list.append( my_data[i][1] )
	b_hold_list.append( my_data[i][2] )
	z_hold_list.append( my_data[i][4])
	if my_data[i][5] == 0:
		z_error_hold_list.append( 0.0001)
	else:
		z_error_hold_list.append( my_data[i][5] )
	if my_data[i][7]==0:
		mu_error_hold_list.append( 0.0001)
	else:
		mu_error_hold_list.append( my_data[i][7] )
l_np_global = np.array(l_hold_list)
b_np_global = np.array(b_hold_list)
sn_position_global = zip(l_np_global, b_np_global)
z_np_data = np.array(z_hold_list) 
z_error_np_global = np.array(z_error_hold_list)
mu_error_np_global = np.array(mu_error_hold_list)
mu_scatter = np.mean(mu_error_np_global) 
number_of_SN = len(l_np_global) #196 Union2.1 with low z accepted from 0<=z<=0.05


############################## 
hist, bins = np.histogram(z_np_data) #standard is bins=np.sqrt(number_of_SN). This will the total number of different z values when done. 1.5 makes it larger, but keeps the distribution form.
bin_midpoints = bins[:-1]+np.diff(bins)/2
cdf = np.cumsum(hist)
print cdf
cdf = 1.0*cdf/cdf[-1] #make it a float between (0,1].
print cdf
# values = np.random.rand(number_of_SN)*(max(bin_midpoints)-min(bin_midpoints))+min(bin_midpoints)
# print values
#old distrete cdf method
# value_bins = np.searchsorted(cdf, values)
# z_np_global = bin_midpoints[value_bins]
cdf_inversed_c = interpolate.interp1d(cdf, bin_midpoints) #makes function where C_VALUE = cdf_inversed_c(CDF_VALUE). Check out CDF_test.py
print cdf_inversed_c(0.7)
values = np.random.rand(number_of_SN)*(max(cdf)-min(cdf))+min(cdf)
z_np_global = cdf_inversed_c(values)
# mu_true = csm.distance_modulus(z_np_global, **cosmo)
# mu_hold = np.empty(number_of_SN)
# for m in range(number_of_SN):
# 	mu_hold[m] = np.array(mu_true[m]+np.random.normal(0,mu_error_np_global[m],1))
# mu_np_global = np.array(mu_hold)


# xnew = np.arange(min(cdf), max(cdf), 0.01)
# ynew = cdf_inversed_c(xnew)
# print cdf
# plt.plot(cdf, bin_midpoints, 'o', xnew, ynew, '-')
# plt.show()


############################## 
f, axarr = plt.subplots(2)
axarr[0].hist(z_np_data)
axarr[0].set_title('data')
axarr[1].hist(z_np_global)
axarr[1].set_title('from hist')
plt.show()