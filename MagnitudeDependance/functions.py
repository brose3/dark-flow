import cosmolopy as cp
import cosmolopy.distance as csd
import cosmolopy.magnitudes as csm
import scipy.optimize as opt
import numpy as np



def chi_sqrd_cosmo(OMfit_0, h_fit_0):                     #define chi-squared
	Omega_Lamda_fitting_0=1-OMfit_0             #OMfit_0 is short for Omega_Matter_fitting_0
	cosmo = {'omega_M_0' : OMfit_0, 'omega_lambda_0' : Omega_Lamda_fitting_0, 'h' : h_fit_0}
	cosmo = csd.set_omega_k_0(cosmo) 
	mu_theory = cp.magnitudes.distance_modulus(z_np_global, **cosmo)
	p = sum( (mu_np_global-mu_theory)**2/(mu_error_np_global**2) )
	return p

def find_z_res(z_func, *args):                         #args: mu_func, z_func, and cosmology (but cosmo works)
	mu_theory_fnczfit = cp.magnitudes.distance_modulus(z_func, **cosmo)
	return (mu_theory_fnczfit - args[0] + 10*np.log10((1+args[1])/(1+z_func)))**2

#minimized function
def function(l, b, z_bulk, OMfit_0, h_fit_0):
	############################## set up variables
	cosmo = {'omega_M_0' : OMfit_0, 'omega_lambda_0' : 1-OMfit_0, 'h' : h_fit_0}
	cosmo = csd.set_omega_k_0(cosmo)
	pole = (l, b)
	############################## Solve for fit_res_np, the value of the cosine fit at each SN's angular distance
	fit_res_np = z_bulk*cos_angdist(pole, sn_position_global)
	############################## Solve for z_res_np_global, realy residue from bulk flow (Davis 2011 eq 15).
	global z_res_np_global
	z_res_np_global = np.zeros(len(z_np_global))
	z_cosmo_measured_np = np.zeros(len(z_np_global))      
	for i, muz in enumerate(zip(mu_np_global,z_np_global)):
		arguments = (muz[0], muz[1], cosmo) 
		zeroed = opt.minimize_scalar(find_z_res, bounds = (0.002, 1), args= arguments, method = 'Bounded') #    
		z_cosmo_measured_np[i] = zeroed.x
	z_res_np_global = (1+z_np_global)/(1+z_cosmo_measured_np)-1 
	############################## Calculate chi-squared
	chi_z_error_np = mu_error_np_global * (np.log(10)/5)*(z_cosmo_measured_np*(1+z_cosmo_measured_np/2)/(1+z_cosmo_measured_np)) #using z_error over mu_error from A4 in Davis 2011
	global outlier_global
	outlier_global = ((fit_res_np-z_res_np_global)**2/(chi_z_error_np**2))
	chi_squared = sum((fit_res_np-z_res_np_global)**2/(chi_z_error_np**2)) #chi_squared of res, but is denomiator correct?

	## save data for debuging results
	global fulldata_np_global
	fulldata_np_global = np.dstack([l_np_global, b_np_global, z_np_global, z_error_np_global, mu_np_global, mu_error_np_global, fit_res_np, z_res_np_global, chi_z_error_np, outlier_global])
	return chi_squared

def cos_angdist(n_bulk,data_sn, unitflag=0):
	#unitflag 1 means degrees
	if unitflag == 1:
		n_bulk[0] = n_bulk[0]* np.pi/180.0
		n_bulk[1] = n_bulk[1] * np.pi/180.0
	#Get cosine(angular distance)
	dots = []
	for i in data_sn:
		dots.append( dotproduct( i, n_bulk) )
	return np.array( dots )

def dotproduct( data, pole, unitflag=0 ):
	#unitflag 1 means degrees, 0 means radians
	return np.cos( offset( unitflag, data[0],pole[0],data[1],pole[1]) )

def offset(unitflag,ra1,ra2,dec1,dec2):
	#unitflag 1 means degrees
	if unitflag == 1:
		ra1 = ra1 * np.pi/180.0
		ra2 = ra2 * np.pi/180.0
		dec1 = dec1 * np.pi/180.0
		dec2 = dec2 * np.pi/180.0
	deldec = dec2 - dec1
	delra  = ra2 - ra1
	sindis = np.sqrt(np.sin(deldec/2.0)*np.sin(deldec/2.0) + np.cos(dec2)*np.cos(dec1)*np.sin(delra/2)*np.sin(delra/2))
	dis = 2.0*np.arcsin(sindis)
	ang = np.arctan(np.sin(delra) / (np.cos(dec1)*np.tan(dec2) - np.sin(dec1)*np.cos(delra)))
	offe = dis * np.sin(ang)
	offn = dis * np.cos(ang)
	offt = np.sqrt(offe**2 + offn**2)                   #off = np.array( offe* (180/pi) * 3600.0,offn* (180/pi) * 3600.0,offt )
	off = np.array( offt )
	return off