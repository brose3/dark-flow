import numpy as np
import matplotlib.pyplot as plt

#old data
LocationFound_02_28 = np.array([
	#0,100,200,300,400,500,1000,1500 km/s
[ (-10,19), (-179,27), (69,35), (55,16), (78,20), (54,17), (58,25), (59,18) ], #sample 0
[ (-22,-18), (55,-13), (48,25), (45,34), (53,26), (60,24), (63,24), (60,21) ], #sample 1
[ (180, 76), (154, 4), (58, 9), (58,20), (75,13), (76,26), (64,18), (65,18) ], #sample 2
[ (156,-6), (116, 1), (58, 2), (50,24), (73, 23), (69,14), (65, 23), (65,20) ], #sample 3
[ (-179,-90), (131,16), (107,-2), (55,32), (62,19), (49,26), (62,18), (67,24)] #sample 4
])

#most recent data, 03-20 & 03-21 run
LocationFound = np.array([
	#0,100,200,300,400,500,1000,1500 km/s
[ (180,-4), (50,19), (69,17), (62,29), (74,11), (72,22), (75,17), (75,15)], 		#sample 0
[ (-170,13), (85,27), (81,23), (92,23), (75,23), (58,18), (59,20), (63,18)],  		#sample 1
[ (135, 2), (86, 3), (85,25), (76,15), (64,23), (82,13), (63,17), (68,23)],   		#sample 2
[ (-8,72), (40,31), (57,27), (75,12), (68,22), (71,14), (74,21), (59,20)],   		#sample 3
[ (138,0), (53,-8), (71,26), (56,17), (66,20), (72,20), (61,18), (68,20)]   		#sample 4
])

#LocationFound is each row is a sample, each colum is a posible bulk flow velocity. Each entry is a tuple of the bulk flow direction found. Direction inputed was: (60,20). Bulk flow velocities are: [0,100,200,300,400,500,1000,1500].
print np.mean(np.rot90(LocationFound,3),1) #outputs mean of l,b over all 5 samples from 0-1500 km/s in that order.


fig1 = plt.figure()
ax1=plt.subplot(211)
lab1 = [0,100,200,300,400,500,1000,1500]
#for i, found in enumerate(LocationFound[:,:,0]):
#	plt.scatter(lab1, found)
plt.plot(lab1, [60,60,60,60,60,60,60,60], ':')
plt.scatter(lab1, LocationFound[0,:,0])#, c='r', label='trial 1')
plt.scatter(lab1, LocationFound[1,:,0])#, c='g', label='trial 2')
plt.scatter(lab1, LocationFound[2,:,0])#, c='b', label='trial 3')
plt.scatter(lab1, LocationFound[3,:,0])#, c='k', label='trial 4')
plt.scatter(lab1, LocationFound[4,:,0])#, c='m', label='trial 5')
plt.axis([-100,1600,-200,200])
#plt.xlabel('sample number')
plt.ylabel(r'$l$ ($^\circ$)')
#plt.legend(loc='best')
#plt.savefig('histtest1.pdf')

#fig2 = plt.figure()
ax2= plt.subplot(212)
lab = ['0 km/s','100 km/s','200 km/s','300 km/s','400 km/s','500 km/s','1000 km/s','1500 km/s']
#for i, found in enumerate(np.rot90(LocationFound,3)[:,:,1]):
#	plt.plot(found, ':*', label = lab[i])
plt.plot(lab1, [20,20,20,20,20,20,20,20], ':')
plt.scatter(lab1, LocationFound[0,:,1])#, c=[(1,0,0),(0,1,0),(0,0,1),(1,.5,.25),(.5,.5,.5),(.25,.4,0),(0.5,0.25,1),(0,0.75,.5)], label='trial 1')
plt.scatter(lab1, LocationFound[1,:,1])#, c=[(1,0,0),(0,1,0),(0,0,1),(1,.5,.25),(.5,.5,.5),(.25,.4,0),(0.5,0.25,1),(0,0.75,.5)], label='trial 2')
plt.scatter(lab1, LocationFound[2,:,1])#, c=[(1,0,0),(0,1,0),(0,0,1),(1,.5,.25),(.5,.5,.5),(.25,.4,0),(0.5,0.25,1),(0,0.75,.5)], label='trial 3')
plt.scatter(lab1, LocationFound[3,:,1])#, c=[(1,0,0),(0,1,0),(0,0,1),(1,.5,.25),(.5,.5,.5),(.25,.4,0),(0.5,0.25,1),(0,0.75,.5)], label='trial 4')
plt.scatter(lab1, LocationFound[4,:,1])#, c=[(1,0,0),(0,1,0),(0,0,1),(1,.5,.25),(.5,.5,.5),(.25,.4,0),(0.5,0.25,1),(0,0.75,.5)], label='trial 5')
#plt.legend(prop={'size':6}, ncol = 2, numpoints = 1, loc='center', bbox_to_anchor=(0.5,0.3))
plt.axis([-100,1600,-100,100])
plt.xlabel(r'bulk flow magnitude (km s$^{-1}$)')
plt.ylabel(r'$b$ ($^\circ$)')
plt.savefig('LBvsMagnitude.pdf')

fig2 = plt.figure()
ax1 = fig2.add_axes([0.1,0.1,0.8,0.8], projection='hammer') #mollweide, or aitoff, hammer. aitoff is not equal area.
plt.grid("on")
#plt.scatter(0,0,marker='*', s=46, label = 'origin of search')
plt.scatter(np.radians(LocationFound[:,0,0]),np.radians(LocationFound[:,0,1]), c='b', label = lab[0])
plt.scatter(np.radians(LocationFound[:,1,0]),np.radians(LocationFound[:,1,1]), c='g', label = lab[1])
plt.scatter(np.radians(LocationFound[:,2,0]),np.radians(LocationFound[:,2,1]), c='r', label = lab[2])
plt.scatter(np.radians(LocationFound[:,3,0]),np.radians(LocationFound[:,3,1]), c='c', label = lab[3])
plt.scatter(np.radians(LocationFound[:,4,0]),np.radians(LocationFound[:,4,1]), c='m', label = lab[4])
plt.scatter(np.radians(LocationFound[:,5,0]),np.radians(LocationFound[:,5,1]), c='y', label = lab[5])
plt.scatter(np.radians(LocationFound[:,6,0]),np.radians(LocationFound[:,6,1]), c='k', label = lab[6])
plt.scatter(np.radians(LocationFound[:,7,0]),np.radians(LocationFound[:,7,1]), c=(0.5,0.25,1), label = lab[7])
plt.scatter(np.radians(60), np.radians(20), marker="*",s=76, c =[(0,1,0)], label = 'true locations')
plt.legend(prop={'size':8}, ncol = 2, loc=4, scatterpoints = 1)
plt.savefig('AllSkyvsMagnitude.pdf')
