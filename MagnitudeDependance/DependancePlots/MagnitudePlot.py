##############################
#Creates: 
#Written by Ben Rose
#brose3@nd.edu
#On 2013-03-24  
#Notes: 
##############################
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
##############################

data_set = 'low' #run through 'low', 'high', 'random', 'perfect'

# if 'low' need to change labes in plot-function

#update fonts, make it LaTeX-ish -- http://stackoverflow.com/questions/21461155/change-matplotlibs-default-font
matplotlib.rc('font', family='serif') 
# matplotlib.rc('font', serif='Computer Modern Roman')    #no idea why this breaks it the code. 
matplotlib.rcParams.update({'font.size': 12})

def plot(reslt_file, axis1_array, axis2_array, samples = 25, bulk_flow_speed_givens = range(0,500,100)+range(500,1501,500)+ [2000, 3000, 5000]):
	def func(v, dv):
		return np.sqrt(v**2 + dv**2)

	output = np.genfromtxt(reslt_file ,delimiter=',')
	output_np_array = np.array(output)

	#trim data, I don't want to remember how many runs, this kees it simple and consistent between different runs.
	if np.shape(output_np_array)[0] > 25:
		output_np_array = output_np_array[:25]
	if np.shape(output_np_array)[1] != len(bulk_flow_speed_givens):
		output_np_array = output_np_array[:,:len(bulk_flow_speed_givens)]
	
	Mean = np.mean(output_np_array, 0)
	Median = np.median(output_np_array, 0)
	Error = np.std(output_np_array, 0)
	
	print bulk_flow_speed_givens
	
	# fig1 = plt.figure(1)
	# ax = fig1.add_subplot(1,1,1, aspect='equal')
	# plt.plot(bulk_flow_speed_givens+[1000+max(bulk_flow_speed_givens)] ,bulk_flow_speed_givens+[1000+max(bulk_flow_speed_givens)] ,'--r')
	# for samplot in range(samples):
	#     plt.plot(bulk_flow_speed_givens,output_np_array[samplot], 'k*', ms=5, alpha=0.5)
	# plt.errorbar(bulk_flow_speed_givens, Mean, Error, fmt='.b', ms=15, alpha=1)
	# plt.xlabel(r'dark flow velocity [km s$^{-1}$]', fontsize=14)
	# plt.ylabel(r'dark flow velocity recovered [km s$^{-1}$]', fontsize=14)
	# plt.axis(axis1_array)

	# print 'Mean', Mean
	# print 'std', Error
	# print Mean - bulk_flow_speed_givens
	# print Median - bulk_flow_speed_givens

	fig2 = plt.figure(2)
	ax = fig2.add_subplot(1,1,1, aspect='equal')
	plt.plot(bulk_flow_speed_givens, Mean - bulk_flow_speed_givens, 'b', label="Residual of Mean")
	plt.plot(bulk_flow_speed_givens, Median - bulk_flow_speed_givens, 'm', label="Residual of Median")
	plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3, ncol=2, mode="expand", borderaxespad=0.)
	plt.axis(axis2_array)

	popt, pcov = curve_fit(func, np.array(bulk_flow_speed_givens), Mean, sigma=Error)
	print popt[0]

	fig3 = plt.figure(3)
	ax = fig3.add_subplot(1,1,1, aspect='equal')
	plt.plot(bulk_flow_speed_givens+[1000+max(bulk_flow_speed_givens)], bulk_flow_speed_givens+[1000+max(bulk_flow_speed_givens)] ,'--r')

	#plot each simulation sample, but only label one for the ledgend
	for samplot in range(samples):
		if samplot == 0:
			plt.plot(bulk_flow_speed_givens,output_np_array[samplot], 'k*', ms=5, alpha=0.5, label='simulation')
		else:
		    plt.plot(bulk_flow_speed_givens,output_np_array[samplot], 'k*', ms=5, alpha=0.5)

	plt.errorbar(bulk_flow_speed_givens, Mean, Error, fmt='.b', ms=15, alpha=1, label='mean $\pm$ std')
	plt.plot(np.arange(0, 1000+max(bulk_flow_speed_givens), 10), 
		func(np.arange(0, 1000+max(bulk_flow_speed_givens), 10), popt[0]), 
		'k', label='$\sqrt{{v_{{df}}^2+{:0.0f}^2}}$'.format(popt[0]))

	plt.xlabel(r'dark flow velocity [km s$^{-1}$]', fontsize=14)
	plt.ylabel(r'dark flow velocity recovered [km s$^{-1}$]', fontsize=14)
	plt.axis(axis1_array)
	plt.legend(loc=0, numpoints=1, fontsize=10)

	return fig2, fig3


if data_set == 'low':
	#2016-02-* is with fast minimization setting
	# data = '2016-02-26-low-Output.csv'
	#2015-0* is with thorough minimization setting
	data = '2015-08-24-low-Output.csv' 

	top = 1550 #note, this method failed past this. I got a bad 5000 value
	axis1 = [-25, 1100, -25, 1100]
	axis2 = [0, top, -200, 200]

	bulk_flow_speed_givens = [0,100,200,300,400,500,1000]
	residual, mag = plot(data, axis1, axis2, bulk_flow_speed_givens=bulk_flow_speed_givens)
	mag.savefig('union2low_mag_25_fit.pdf')

elif data_set == 'high':
	# data = '2016-02-26-high-Output.csv'
	data = '2015-08-24-high-Output.csv'
	axis1 = [-25, 5300, -25, 9500]
	axis2 = [0, 5000, -100, 2000]

	residual, mag = plot(data, axis1, axis2)
	mag.savefig('union2high_mag_25_fit.pdf')

elif data_set == 'perfect':
	# data = '2016-02-26-perfect-Output.csv'
	data = '2015-08-26-perfect-Output.csv'
	# axis1 = [-25, 5300, -25, 5500]    #full data set
	axis1 = [-25, 1100, -25, 1300]    #out to 1000 km/s
	# axis1 = [-25, 1600, -25, 1750]    #out to 1500 km/s
	axis2 = [0, 5000, -500, 500]

	bulk_flow_speed_givens = [0,100,200,300,400,500,1000]
	residual, mag = plot(data, axis1, axis2, bulk_flow_speed_givens=bulk_flow_speed_givens)
	mag.savefig('perfect_mag_25_fit.pdf')

elif data_set == 'fixed':
	# data ='2015-09-21-highfixed-Output.csv'
	data = '2015-09-23-highfixed-Output.csv' #from 2015-09-21-highfixed-Output.csv + 7000 & 9000 from 2015-09-22-161130* 
	axis1 = [-25, 9500, -25, 11500]
	axis2 = [0, 9000, -100, 2000]

	residual, mag = plot(data, axis1, axis2, bulk_flow_speed_givens = range(0,500,100)+range(500,1501,500)+ [2000, 3000, 5000, 7000, 9000])
	# mag.savefig('Union2higFixedhMag_25_fit20160210.pdf')

elif data_set == 'random':
	# data ='2016-02-26-highRandom-Output.csv'
	data ='2015-09-23-highRandom-Output.csv'
	# axis1 = [-25, 7500, -25, 10000] #use if you want all of the data taken
	axis1 = [-25, 5300, -25, 6500]
	axis2 = [0, 7000, -100, 1000]

	residual, mag = plot(data, axis1, axis2, bulk_flow_speed_givens = range(0,500,100)+range(500,1501,500)+ [2000, 3000, 5000, 7000])
	mag.savefig('union2high_randomsky_mag_25_fit.pdf')

else:
	print 'not a correct'

# plt.show()