##############################
#Creates: 
#Written by Ben Rose
#brose3@nd.edu
#On 2013-11-25  
#Notes: 
##############################
import cosmolopy as cp
import cosmolopy.distance as csd
import cosmolopy.magnitudes as csm
import minuit
import scipy.optimize as opt
import numpy as np
import matplotlib.pyplot as plt
import sys
import time
import scipy.interpolate as intt
#from functions import *
#from parallelize import parallelize, per_item
##############################


############################## What Data Cuts? ############################## 
samples = 3
z_max = 2 #
z_min = 0.05 #0.003 is lowest in Union2.1 with low z cut accepted, 0.00334 for 1000km/s
z_type = 5 #0=uniform from min to max, 1=beta to match Union2.1 z_max~0.05, 2=normal from min to max, 3=SDSS Weibull, 4=SDSS Normal, 5=From Histogram
bulk_flow_speed_givens = range(0,500,100)+range(500,1501,500)+ [2000, 3000, 5000] #in km/s
bulk_flow_direction= (np.radians(290), np.radians(20)) #in galactic coordinates, radians #This is from \cite{Dai:2011fb} (290,20)
#bulk_flow_direction= (np.radians(46), np.radians(-23.86)) #sdss stripe edge (-50 ra, 0 dec)
#bulk_flow_direction= (np.radians(189.26), np.radians(-38.07)) #sdss stripe edge (59 ra, 0 dec)
om_set, ol_set, h_set = 0.2833, 0.7166, 0.6987 #for z<3 from Union2.1
cosmo = {'omega_M_0' : om_set, 'omega_lambda_0' : ol_set, 'h' : h_set}
cosmo = csd.set_omega_k_0(cosmo) 
date = time.strftime("%Y-%m-%d-%H%M%S") 
crc = True
if crc:
	home = '/afs/crc.nd.edu/user/b/brose3/Private/dark-flow/MagnitudeDependance/'
else:
	home = ''
##############################


############################## Functions ############################## 
def chi_sqrd_cosmo(OMfit_0, h_fit_0):                     #define chi-squared
	Omega_Lamda_fitting_0=1-OMfit_0             #OMfit_0 is short for Omega_Matter_fitting_0
	cosmo = {'omega_M_0' : OMfit_0, 'omega_lambda_0' : Omega_Lamda_fitting_0, 'h' : h_fit_0}
	cosmo = csd.set_omega_k_0(cosmo) 
	mu_theory = cp.magnitudes.distance_modulus(z_np_global, **cosmo)
	p = sum( (mu_np_global-mu_theory)**2/(mu_error_np_global**2) )
	return p
def function(l, b, z_bulk, OMfit_0, h_fit_0):
	############################## needed functions inside
	def cos_angdist(n_bulk,data_sn, unitflag=0):
		#unitflag 1 means degrees
		if unitflag == 1:
			n_bulk[0] = n_bulk[0]* np.pi/180.0
			n_bulk[1] = n_bulk[1] * np.pi/180.0
		#Get cosine(angular distance)
		dots = []
		for i in data_sn:
			dots.append( dotproduct( i, n_bulk) )
		return np.array( dots )
	def dotproduct( data, pole, unitflag=0 ):
		#unitflag 1 means degrees, 0 means radians
		return np.cos( offset( unitflag, data[0],pole[0],data[1],pole[1]) )
	def offset(unitflag,ra1,ra2,dec1,dec2):
		#unitflag 1 means degrees
		if unitflag == 1:
			ra1 = ra1 * np.pi/180.0
			ra2 = ra2 * np.pi/180.0
			dec1 = dec1 * np.pi/180.0
			dec2 = dec2 * np.pi/180.0
		deldec = dec2 - dec1
		delra  = ra2 - ra1
		sindis = np.sqrt(np.sin(deldec/2.0)*np.sin(deldec/2.0) + np.cos(dec2)*np.cos(dec1)*np.sin(delra/2)*np.sin(delra/2))
		dis = 2.0*np.arcsin(sindis)
		ang = np.arctan(np.sin(delra) / (np.cos(dec1)*np.tan(dec2) - np.sin(dec1)*np.cos(delra)))
		offe = dis * np.sin(ang)
		offn = dis * np.cos(ang)
		offt = np.sqrt(offe**2 + offn**2)                   #off = np.array( offe* (180/pi) * 3600.0,offn* (180/pi) * 3600.0,offt )
		off = np.array( offt )
		return off
	############################## set up variables
	cosmo = {'omega_M_0' : OMfit_0, 'omega_lambda_0' : 1-OMfit_0, 'h' : h_fit_0}
	cosmo = csd.set_omega_k_0(cosmo)
	pole = (l, b)
	############################## Solve for fit_res_np, the value of the cosine fit at each SN's angular distance
	fit_res_np = z_bulk*cos_angdist(pole, sn_position_global)
	
	############################## Solve for z_res_np_global, realy residue from bulk flow (Davis 2011 eq 15).
	def find_z_res(z_func, *args):                         #args: mu_func, z_func, and cosmology (but cosmo works)
		mu_theory_fnczfit = cp.magnitudes.distance_modulus(z_func, **cosmo)
		return (mu_theory_fnczfit - args[0] + 10*np.log10((1+args[1])/(1+z_func)))**2
	global z_res_np_global
	z_res_np_global = np.zeros(len(z_np_global))
	z_cosmo_measured_np = np.zeros(len(z_np_global))      
	for i, muz in enumerate(zip(mu_np_global,z_np_global)):
		arguments = (muz[0], muz[1], cosmo) 
		zeroed = opt.minimize_scalar(find_z_res, bounds = (0.002, 1), args= arguments, method = 'Bounded') #    
		z_cosmo_measured_np[i] = zeroed.x
	z_res_np_global = (1+z_np_global)/(1+z_cosmo_measured_np)-1 
	
	############################## Calculate chi-squared
	mu_to_z_error_np = mu_error_np_global * (np.log(10)/5)*(z_cosmo_measured_np*(1+z_cosmo_measured_np/2)/(1+z_cosmo_measured_np)) #using z_error over mu_error from A4 in Davis 2011
	chi_z_error_np = mu_to_z_error_np**2 + (3/299792)**2 # add in quadrature a 300 km/s z error to deweight any peculiar motion.
	global outlier_global
	outlier_global = ((fit_res_np-z_res_np_global)**2/chi_z_error_np)
	chi_squared = sum((fit_res_np-z_res_np_global)**2/chi_z_error_np) #chi_squared of res, but is denomiator correct?

	## save data for debuging results
	global fulldata_np_global
	fulldata_np_global = np.dstack([l_np_global, b_np_global, z_np_global, z_error_np_global, mu_np_global, mu_error_np_global, fit_res_np, z_res_np_global, chi_z_error_np, outlier_global])
	
	return chi_squared
def cos_angdist(n_bulk,data_sn, unitflag=0):
	#unitflag 1 means degrees
	if unitflag == 1:
		n_bulk[0] = n_bulk[0]* np.pi/180.0
		n_bulk[1] = n_bulk[1] * np.pi/180.0
	#Get cosine(angular distance)
	dots = []
	for i in data_sn:
		dots.append( dotproduct( i, n_bulk) )
	return np.array( dots )
def dotproduct( data, pole, unitflag=0 ):
	#unitflag 1 means degrees, 0 means radians
	return np.cos( offset( unitflag, data[0],pole[0],data[1],pole[1]) )
def offset(unitflag,ra1,ra2,dec1,dec2):
	#unitflag 1 means degrees
	if unitflag == 1:
		ra1 = ra1 * np.pi/180.0
		ra2 = ra2 * np.pi/180.0
		dec1 = dec1 * np.pi/180.0
		dec2 = dec2 * np.pi/180.0
	deldec = dec2 - dec1
	delra  = ra2 - ra1
	sindis = np.sqrt(np.sin(deldec/2.0)*np.sin(deldec/2.0) + np.cos(dec2)*np.cos(dec1)*np.sin(delra/2)*np.sin(delra/2))
	dis = 2.0*np.arcsin(sindis)
	ang = np.arctan(np.sin(delra) / (np.cos(dec1)*np.tan(dec2) - np.sin(dec1)*np.cos(delra)))
	offe = dis * np.sin(ang)
	offn = dis * np.cos(ang)
	offt = np.sqrt(offe**2 + offn**2)                   #off = np.array( offe* (180/pi) * 3600.0,offn* (180/pi) * 3600.0,offt )
	off = np.array( offt )
	return off
##############################


############################## Universal ############################## 
# Import Union2.1 placements
# my_data = np.recfromcsv(home+'Union2.1_wLowZ.csv')
#my_data = np.recfromcsv(home+'sdss-ii_lb_cmb.csv')
#my_data = np.recfromcsv(home+'sdss-ii_lb_cmb_0.15.csv')
# my_data = np.recfromcsv(home+'Best_5000.csv')
<<<<<<< HEAD
my_data = np.recfromcsv(home+'z_cut_1_mu_scatter_0.05_bf_0_sn_500.csv')
=======
# my_data = np.recfromcsv(home+'z_cut_1_mu_scatter_0.23_bf_0_sn_500.csv')
my_data = np.recfromcsv(home+'Best_500_2014-08-08.csv')
>>>>>>> f71474d183db6184506fc468e6ee72d4326a6b83
## Universal Data
l_hold_list=[] #hold
b_hold_list=[]
z_hold_list=[]
z_error_hold_list=[] #error, hold
mu_error_hold_list=[] #error, hold
for i in range(len(my_data)):
	if my_data[i][7] > 0.4: #only take distance error less then 0.4 mag
		continue
	elif my_data[i][4] > z_max:
		continue
	elif my_data[i][4] < z_min:
		continue
	else:
		if my_data[i][1] > np.pi:
			l_hold_list.append( my_data[i][1] -2* np.pi)
		else:
			l_hold_list.append( my_data[i][1] )
		b_hold_list.append( my_data[i][2] )
		z_hold_list.append( my_data[i][4] )
		if my_data[i][5] == 0:
			z_error_hold_list.append( 0.0001)
		else:
			z_error_hold_list.append( my_data[i][5] )
		if my_data[i][7]==0:
			mu_error_hold_list.append( 0.0001)
		else:
			mu_error_hold_list.append( my_data[i][7] )
global l_np_global
l_np_global = np.array(l_hold_list)
global b_np_global
b_np_global = np.array(b_hold_list)
global sn_position_global 
sn_position_global = zip(l_np_global, b_np_global)
z_np_data = np.array(z_hold_list)
global z_error_np_global 
z_error_np_global = np.array(z_error_hold_list)
global mu_error_np_global
mu_error_np_global = np.array(mu_error_hold_list)
mu_scatter = np.mean(mu_error_np_global) 
number_of_SN = len(l_np_global) #196 Union2.1 with low z accepted from 0<=z<=0.05
## Specific Data placeholders
z_np_array = np.empty((samples, len(bulk_flow_speed_givens), number_of_SN ))
mu_np_array = np.empty((samples, len(bulk_flow_speed_givens), number_of_SN ))
output_np_array = np.empty((samples, len(bulk_flow_speed_givens)))
direction_np_array = np.empty((samples, len(bulk_flow_speed_givens)), dtype = object ) #so that it can hold a tuple
global z_np_global
global mu_np_global
##############################


############################## Run Specific ##############################
# Run over all samples & bulk flows, double parallel loops sounds bad
for j, samp in enumerate(range(samples)):
	#k=0
	for k, v in enumerate(bulk_flow_speed_givens):#, fork=per_item): #for tests with parallelize.py
		## Make random z & mu sets
		if z_type == 0:
			z_cosmo = np.random.rand(number_of_SN)*(z_max-z_min)+z_min #ranodm is uniform over[z_min,z_cutoff)
		elif z_type == 1:
			z_cosmo = np.random.beta(3.5158,153.1671,number_of_SN) #ranodm beta to match Union2.1 with low z accepted and z_max=0.05
		elif z_type == 2:
			z_cosmo = np.random.randn(number_of_SN)*(z_max-z_min)+z_min #Normal dist from [0,1) then shifted to [z_min,z_cutoff).
		elif z_type == 3:
			z_cosmo = 0.29765*np.random.weibull(3.57697,number_of_SN)
		elif z_type == 4:
			z_cosmo = 0.0857*np.random.randn(number_of_SN)+0.2678
		elif z_type == 5:
			hist, bins = np.histogram(z_np_data)
			bin_midpoints = bins[:-1]+np.diff(bins)/2
			cdf = np.cumsum(hist)
			cdf = 1.*cdf/cdf[-1] #make it a float between (0,1].cd ..
			values = np.random.rand(number_of_SN)*(max(cdf)-min(cdf))+min(cdf) #cdf does not start at 0.
			cdf_inversed_c = intt.interp1d(cdf, bin_midpoints) #makes function where C_VALUE = cdf_inversed_c(CDF_VALUE). Check out CDF_test.py
			z_cosmo = cdf_inversed_c(values)
			#old method that does not produce continoual values for z_cosmo
			# value_bins = np.searchsorted(cdf, values)
			# z_cosmo = bin_midpoints[value_bins]
		else:
			sys.exit("Improper z-type. Please choose 0, 1, or 2 for uniform, beta, or normal. More have been added.")
		z_np_global = np.array( (1 + v*cos_angdist(bulk_flow_direction, sn_position_global)/299792)*(1 + z_cosmo) - 1)
		for i, red in enumerate(z_np_global):
			if red<0.00287: #closer thqn Virgo, http://adsabs.harvard.edu/abs/1988A%26A...202...70A
				z_np_global[i] = 0.00287
		mu_true = csm.distance_modulus(z_cosmo, **cosmo) + (10*np.log10(1+ v*cos_angdist(bulk_flow_direction, sn_position_global)/299792))
		mu_hold = np.empty(number_of_SN)
		for m in range(number_of_SN):
			mu_hold[m] = np.array(mu_true[m]+np.random.normal(0,mu_error_np_global[m],1))
		mu_np_global = np.array(mu_hold)

		### Save Results
		data = np.dstack((l_np_global,b_np_global,z_np_global,z_error_np_global,mu_np_global,mu_error_np_global))
		np.savetxt(home+'Data/'+date+'-Data-Sample'+str(samp)+'-BulkFlow'+str(v)+'.csv',data[0], delimiter=',')
		print 'made file '+date+'-Data-Sample'+str(samp)+'-BulkFlow'+str(v)+'.csv'
		z_np_array[j,k,:] = z_np_global
		mu_np_array[j,k,:] = mu_np_global

		## Test data sets
		fullmin = minuit.Minuit(function)
		fullmin.up = 1 #1 sigma changes result by up, use 0.001? if using mu_error in chi-square not a good z_error
		fullmin.limits["l"] = (-np.pi, np.pi)
		fullmin.limits["b"] = (-np.pi/2, np.pi/2)
		fullmin.limits["z_bulk"] = (0, 10)
		fullmin.limits["OMfit_0"] = (0.2,0.4)
		fullmin.limits["h_fit_0"] = (0.6,0.88)
		fullmin.values["OMfit_0"] = 0.20
		fullmin.values["h_fit_0"] = 0.7
		try:
			fullmin.strategy=2
			#fullmin.tol = 1
			fullmin.printMode = 1 #0 is no output, 1 is fcn & parameter values at each call
			fullmin.migrad()
			fullmin_error='no error'
		except minuit.MinuitError as me:
			if me == 'Covariance is not positive definite.':
				print '\n\n\n\n RUNNING WITHOUT LIMIT \n\n\n\n'
				fullmin.maxcalls = None #default is maxcalls = None
				try:
					fullmin.migrad()
				except minuit.MinuitError as me:
					fullmin_error = str(me)
			else:       
				fullmin_error = str(me)
		except:
			fullmin_error='error of unknown kind'

############################## Output Data ##############################
		bulk_direction = [fullmin.values["l"], fullmin.values["b"] ]
		result = (
			"bulk velocity: "+ str(fullmin.values["z_bulk"]*299792)+" +- "+ str(fullmin.errors["z_bulk"]*299792)+ " km/s, "+str(fullmin.values["z_bulk"]),
			"bulk direction: "+ str(np.degrees(bulk_direction))+" in galactic coordinates",
			"cosmology: "+ str(fullmin.values["h_fit_0"]) +', '+ str(fullmin.values["OMfit_0"])+', & '+str(1-fullmin.values["OMfit_0"]),
			"errors are: "+ str(fullmin.errors),
			"final chi-square value: "+ str(fullmin.fval),
			str(fullmin_error),
			"estimated distance to minimum: "+str(fullmin.edm)
			)
		resultshort = ( 
			str(fullmin.values["z_bulk"]*299792)+" +- "+ str(fullmin.errors["z_bulk"]*299792),
			str(np.degrees(bulk_direction)),
			str(fullmin.values["h_fit_0"])+', '+ str(fullmin.values["OMfit_0"])+',  '+str(1-fullmin.values["OMfit_0"]),
			str(fullmin.errors),
			str(fullmin.fval),
			str(fullmin_error),
			str(fullmin.edm)
			)
		### Save to standard out
		for res in result:
			print res
		### Save data to text file
		resultsaveto = home+'Results/'+date+'-Results-Sample'+str(samp)+'-BulkFlow'+str(v)+'.txt'
		np.savetxt(resultsaveto, resultshort, fmt="%s")
		### Save results to variable
		output_np_array[j,k] = fullmin.values["z_bulk"]*299792
		direction_np_array[j,k] = bulk_direction
		### Save data to plot
		fig1 = plt.figure()
		ax1 = fig1.add_axes([0.1,0.1,0.8,0.8], projection='aitoff')   #lambert,aitoff
		plt.grid("on")
		plt.scatter(l_np_global,b_np_global,c=z_np_global)#, cmap = cm.jet)
		plt.xlabel(r'$l$')
		plt.ylabel(r'$b$')
		plt.colorbar()
		plt.savefig(home+'Data/'+date+'DataUsed-Sample'+str(samp)+'-BulkFlow'+str(v)+'.pdf')
		### Save results to plot
		fig2 = plt.figure() #fig1 is dataset used
		#x-axis => angular dist of SN from bulk flow, y-axis => z_res, sn
		SN_theta_plot = np.arccos(cos_angdist(bulk_direction, sn_position_global))
		plt.scatter(SN_theta_plot, z_res_np_global, c=z_np_global)#, cm.jet )
		plt.colorbar()
		#x-axis => angle, y-axis => cos(angle)
		theta_plot = np.arange(0,np.pi, 0.01)
		cosine_plot = (fullmin.values["z_bulk"])*(np.cos(theta_plot))
		plt.plot(theta_plot, cosine_plot)
		plt.xlabel(r'angular distance on the sky (radians)')
		plt.ylabel(r'z$_{residue}$ (observed - Hubble)')
		plt.savefig(home+'Results/'+date+'ScatterPlot-Sample'+str(samp)+'-BulkFlow'+str(v)+'.pdf')
		#k+=1
	### Save results per Sample, re-writes ever sample, to file
	np.savetxt(home+'Results/'+date+'-Output.csv',output_np_array, delimiter=',')

### Save final results to standard out
Mean = np.mean(output_np_array,0)
Error = np.std(output_np_array,0)
print output_np_array
print Mean
print direction_np_array
### Save final results to file
np.savetxt(home+'Results/'+date+'-Direction.csv',str(direction_np_array), delimiter=',')
np.savetxt(home+'Results/'+date+'-Output.csv',output_np_array, delimiter=',')
##############################


############################## Graph Data ##############################
fig3 = plt.figure()
plt.plot(bulk_flow_speed_givens,bulk_flow_speed_givens,'--r')
for samplot in range(samples):
    plt.plot(bulk_flow_speed_givens,output_np_array[samplot], 'k*', ms=5, alpha=0.5)
plt.errorbar(bulk_flow_speed_givens, Mean, Error, fmt='.b', ms=15, alpha=1)
top= max(bulk_flow_speed_givens)+50
plt.axis([-25, top, -25, top+100])
plt.xlabel(r'bulk velocity (km s$^{-1}$)')
plt.ylabel(r'bulk velocity recovered (km s$^{-1}$)')
save = home+'Results/'+date+'-MagnitudeDependance'
plt.savefig(save+'.pdf')


# fig4 = plt.figure()
# ax1=plt.subplot(211)
# lab1 = [0,100,200,300,400,500,1000,1500]
# plt.plot(lab1, [60,60,60,60,60,60,60,60], ':')
# plt.scatter(lab1, direction_np_array[0,:,0])
# plt.scatter(lab1, direction_np_array[1,:,0])
# plt.scatter(lab1, direction_np_array[2,:,0])
# plt.scatter(lab1, direction_np_array[3,:,0])
# plt.scatter(lab1, direction_np_array[4,:,0])
# plt.axis([-100,1600,-200,200])
# plt.ylabel(r'$l$ ($^\circ$)')

# ax2= plt.subplot(212)
# lab = ['0 km/s','100 km/s','200 km/s','300 km/s','400 km/s','500 km/s','1000 km/s','1500 km/s']
# plt.plot(lab1, [20,20,20,20,20,20,20,20], ':')
# plt.scatter(lab1, direction_np_array[0,:,1])
# plt.scatter(lab1, direction_np_array[1,:,1])
# plt.scatter(lab1, direction_np_array[2,:,1])
# plt.scatter(lab1, direction_np_array[3,:,1])
# plt.scatter(lab1, direction_np_array[4,:,1])
# plt.axis([-100,1600,-100,100])
# plt.xlabel(r'bulk flow magnitude (km s$^{-1}$)')
# plt.ylabel(r'$b$ ($^\circ$)')
# plt.savefig('LBvsMagnitude.pdf')
##############################
