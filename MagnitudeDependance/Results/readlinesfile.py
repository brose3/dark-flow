#get direction or second line of bulk flow output files.
'''file example:
2741.40644974 +- 169.17779896
[-71.96357804  16.03172872]
0.721334902479, 0.271031704479,  0.728968295521
{'OMfit_0': 0.0008398234919008707, 'b': 0.05080512358380007, 'l': 0.06537784439148786, 'h_fit_0': 0.00020029924864012338, 'z_bulk': 0.0005643172564992471}
11044.9363517
Covariance is not positive definite.
'''
''' issues with second line:
spaces before and after [] 
1-3 spaces inbetween numbers
'''
import glob
import pdb

directions = []
# files = ['2014-07-14-110512-Results-Sample4-BulkFlow3000.txt', '2014-07-14-110512-Results-Sample3-BulkFlow3000.txt']
files = glob.glob('2014-07-14-110512-Results-Sample?-BulkFlow0.txt')
print files[1]
for j, run in enumerate(files):
	# if run == "2014-07-14-110512-Results-Sample2-BulkFlow3000.txt":
		# pdb.set_trace()
	f = open(run)
	for i, line in enumerate(f):
		if i == 1:
			hold = line.split(' ') # splits from
			if len(hold[-1]) < 3 and len(hold[0]) < 3: #if last hold is just a "]\n" and the first is just "["
				hold2 = '['+ hold[1]+', '+hold[-2]+']'
			elif len(hold[-1]) < 3:
				hold2 = hold[0]+', '+hold[-2]+']'
			elif len(hold[0])<3:
				hold2 = '['+ hold[1]+', '+hold[-1]
			else: #normal
				hold2 = hold[0]+', '+hold[-1]
			direction = eval(hold2)
			break
	print run
	directions.append(direction) # save into array

for i in zip(files, directions):
	print i
print directions
